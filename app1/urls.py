from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from app1.views import ola
from app1.views import data_atual, data_mais

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'app1.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
    url(r'^ola/$', ola),
    url(r'^$', ola),
    url(r'^hora/$', data_atual),
    url(r'^data/(\d+)/$', data_mais),
)
