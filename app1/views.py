# coding=utf-8
from django.http import HttpResponse , Http404
#from django.template.loader import get_template
#from django.template import Context
from django.shortcuts import render
import datetime

def ola(request):
    return HttpResponse("Olá mundo!")


def data_atual(request):
    now = datetime.datetime.now()
    return render(request, 'data_atual.html', {'data_atual': now})
'''
função render recebe 3 parametros,
1 - é a requisição
2 - é o arquivo de template
3 - é o dicionario com as variaveis que serão substituidas no template
a função retorna um HttpResponse.

'''



def data_mais(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(days=offset)
    html = "<em>Em %s dia(s), será %s.</em>" % (offset, dt)
    return HttpResponse(html)